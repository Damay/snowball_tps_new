// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "SnowBall_TPS.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		USnowBallTPS_StateEffect* myEffect = Cast<USnowBallTPS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsCanAdd = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsCanAdd = true;
					USnowBallTPS_StateEffect* NewEffect = NewObject<USnowBallTPS_StateEffect>(TakeEffectActor, AddEffectClass);
					if (NewEffect)
					{
						ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(TakeEffectActor);

						if (myInterface)
						{
							if (myInterface->CanAddStateEffect(AddEffectClass))
							{
								NewEffect->InitObject(TakeEffectActor, NameBoneHit);
							}
						}
					}
				}
				i++;
			}
		}
	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ParticleSystem, AActor* Target, FVector Offset, FName SocketName)
{
	if (Target)
	{
		FName NameBoneToAttach = SocketName;
		FVector Loc = Offset;

		ACharacter* myCharacter = Cast<ACharacter>(Target);
		if (myCharacter && myCharacter->GetMesh())
		{
			UParticleSystemComponent* myParticalSystem = UGameplayStatics::SpawnEmitterAttached(ParticleSystem, myCharacter->GetMesh(), NameBoneToAttach,
				Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			if (Target->GetRootComponent())
			{
				UParticleSystemComponent* myParticalSystem = UGameplayStatics::SpawnEmitterAttached(ParticleSystem, Target->GetRootComponent(), NameBoneToAttach,
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
}
