// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "Character/SnowBall_TPSCharHealthComponent.h"
#include "Net/UnrealNetwork.h"

bool USnowBallTPS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	NameBone = NameBoneHit;

	ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}


void USnowBallTPS_StateEffect::DestroyObject()
{
	ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool USnowBallTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;

	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void USnowBallTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void USnowBallTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		USnowBall_TPSHealthComponent* myHeathComp = Cast<USnowBall_TPSHealthComponent>(myActor->GetComponentByClass(USnowBall_TPSHealthComponent::StaticClass()));
		if (myHeathComp)
		{
			myHeathComp->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

bool USnowBallTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &USnowBallTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &USnowBallTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	//if (ParticalEffect)
	//{
	//	if (myActor)
	//	{
	//		const ISnowBallTPS_IGameActor* myInterface = Cast<ISnowBallTPS_IGameActor>(myActor);

	//		if (myInterface || Actor->GetClass()->ImplementsInterface(USnowBallTPS_IGameActor::StaticClass()))
	//		{
	//			const FTransform EmitterTransform;

	//			USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	//			if (myMesh)
	//			{
	//				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticalEffect, myMesh, NameBoneHit,
	//					EmitterTransform.GetLocation(), EmitterTransform.Rotator(), EAttachLocation::SnapToTarget, false);


	//			}
	//			else
	//			{
	//				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticalEffect, myActor->GetRootComponent(), NameBoneHit,
	//					EmitterTransform.GetLocation(), EmitterTransform.Rotator(), EAttachLocation::SnapToTarget, false);

	//			}

	//		}
	//	}
	//}
	return true;
}

void USnowBallTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	//if (ParticleEmitter)
	//{
	//	ParticleEmitter->DestroyComponent();
	//}

	//ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void USnowBallTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		USnowBall_TPSHealthComponent* myHeathComp = Cast<USnowBall_TPSHealthComponent>(myActor->GetComponentByClass(USnowBall_TPSHealthComponent::StaticClass()));
		if (myHeathComp)
		{
			myHeathComp->ChangeHealthValue_OnServer(Power);
		}
	}
}

void USnowBallTPS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USnowBallTPS_StateEffect, NameBone);

}
