// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSnowBall_TPS, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogSnowBall_TPS_Net, Log, All);
