// Copyright Epic Games, Inc. All Rights Reserved.
using UnrealBuildTool;

public class SnowBall_TPS : ModuleRules
{
	public SnowBall_TPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {"PhysicsCore", "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
