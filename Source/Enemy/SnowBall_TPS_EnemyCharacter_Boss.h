// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemy/SnowBall_TPS_EnemyCharacterBase.h"
#include "SnowBall_TPS_EnemyCharacter_Boss.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_EnemyCharacter_Boss : public ASnowBall_TPS_EnemyCharacterBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess) override;

	virtual void StartAttackEnemy() override;

	virtual void CharacterDead() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsAlive = true;
};
