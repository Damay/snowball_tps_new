// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "SnowBall_TPS_EnemySpawnBase.generated.h"

UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_EnemySpawnBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnowBall_TPS_EnemySpawnBase();

	/** Returns BoxComponent subobject **/
	FORCEINLINE class UBoxComponent* GetBoxComponent() const { return BoxComponent; }

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Spawn, meta = (AllowPrivateAccess = "true"))
		UBoxComponent* BoxComponent;

protected:
	// Called when the game starts or when spawned 
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	UFUNCTION(BlueprintCallable, Category = "Spawn")
		void AddSpawnEnemyBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		FName Phase = "None";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		int32 CurrentPhase = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		bool bIsPoint = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		bool bIsBoss = false;
};
