// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy/SnowBall_TPS_EnemySpawnBase.h"
#include "Game/SnowBall_TPSGameMode.h"
#include "Kismet/KismetStringLibrary.h"
#include "UObject/NameTypes.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ASnowBall_TPS_EnemySpawnBase::ASnowBall_TPS_EnemySpawnBase()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	GetBoxComponent()->InitBoxExtent(FVector(100.0f, 100.0f, 1.0f));
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnowBall_TPS_EnemySpawnBase::BeginPlay()
{
	Super::BeginPlay();

	AddSpawnEnemyBase();
}

// Called every frame
void ASnowBall_TPS_EnemySpawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnowBall_TPS_EnemySpawnBase::AddSpawnEnemyBase()
{
	if (bIsBoss)
	{
		ASnowBall_TPSGameMode* GameMode = Cast<ASnowBall_TPSGameMode>(UGameplayStatics::GetGameMode(this));

		if (GameMode)
		{
			GameMode->SpawnBoss = this;
		}
	}
	else
	{
		TArray<FString> ArraySpawnBase = UKismetStringLibrary::GetCharacterArrayFromString(UKismetStringLibrary::Conv_NameToString(Phase));

		int i = 0;
		while (i < ArraySpawnBase.Num())
		{
			if (UKismetStringLibrary::IsNumeric(ArraySpawnBase[i]))
			{
				CurrentPhase = UKismetStringLibrary::Conv_StringToInt(ArraySpawnBase[i]);
			}
			i++;
		}

		ASnowBall_TPSGameMode* myGameMode = Cast<ASnowBall_TPSGameMode>(UGameplayStatics::GetGameMode(this));

		if (myGameMode)
		{
			switch (CurrentPhase)
			{
			case 0:
				myGameMode->SpawnBasePhase0.Add(this);
				break;
			case 1:
				myGameMode->SpawnBasePhase1.Add(this);
				break;
			case 2:
				myGameMode->SpawnBasePhase2.Add(this);
				break;
			default:
				break;
			}
		}
	}
}

