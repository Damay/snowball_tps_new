// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy/SnowBall_TPS_EnemyCharacter_Boss.h"
#include "Game/SnowBall_TPS_GameStateBase.h"

void ASnowBall_TPS_EnemyCharacter_Boss::BeginPlay()
{
	Super::BeginPlay();

	DistanceToAttack = 220.0f;
}

void ASnowBall_TPS_EnemyCharacter_Boss::AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess)
{
	//SetStateMove(false);
	SetStateAttack(true);
}

void ASnowBall_TPS_EnemyCharacter_Boss::StartAttackEnemy()
{
	SetStateMove(true);

	Super::StartAttackEnemy();
}

void ASnowBall_TPS_EnemyCharacter_Boss::CharacterDead()
{
	if (HasAuthority())
	{
		bIsAlive = false;

		ASnowBall_TPS_GameStateBase* myGameState = Cast<ASnowBall_TPS_GameStateBase>(GetWorld()->GetGameState());

		if (GetCharacterMovement())
		{
			GetCharacterMovement()->StopMovementImmediately();
		}

		ASnowBall_TPS_PlayerState* CurrentPlayerState = Cast<ASnowBall_TPS_PlayerState>(LastDamageInstigator->GetPlayerState<APlayerState>());
		if (CurrentPlayerState)
		{
			UE_LOG(LogTemp, Warning, TEXT("ASnowBall_TPS_EnemyCharacterBase::CharacterDead - PlayerState is null"));
		}

		if(CurrentPlayerState)
			CurrentPlayerState->SetScore(CurrentPlayerState->GetScore() + KillScoreComponent->Score);

		if (myGameState)
		{
			myGameState->bIsBossDead = true;
		}

		if (IsValid(GetController()))
		{
			GetController()->UnPossess();
		}

		SetLifeSpan(20.0f);
		SetStateMove(false);
		SetStateAttack(false);

		if (IsValid(GetDropItemComponent()))
		{
			GetDropItemComponent()->Drop();
		}

		MulticastAnimPlay(DeadMontage);

		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetCollisionProfileName("PhysicsActor", true);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel3, ECollisionResponse::ECR_Ignore);
	}
}
