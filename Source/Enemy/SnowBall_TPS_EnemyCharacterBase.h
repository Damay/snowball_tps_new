// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Component/SnowBall_TPSHealthComponent.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "FuncLibrary/Types.h"
#include "Component/SnowBall_TPS_KillScoreComp.h"
#include "Component/SnowBall_TPS_DropItemComp.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Game/SnowBall_TPS_PlayerState.h"
#include "SnowBall_TPS_EnemyCharacterBase.generated.h"

UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_EnemyCharacterBase : public ACharacter, public ISnowBallTPS_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASnowBall_TPS_EnemyCharacterBase();

	//HealthComponent
	FORCEINLINE class USnowBall_TPSHealthComponent* GetHealthComponent() const { return HealthComponent; }
	//KillScoreComponent
	FORCEINLINE class USnowBall_TPS_KillScoreComp* GetKillScoreComponent() const { return KillScoreComponent; }
	//DropItemComponent
	FORCEINLINE class USnowBall_TPS_DropItemComp* GetDropItemComponent() const { return DropItemComponent; }

	
private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class USnowBall_TPSHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class USnowBall_TPS_DropItemComp* DropItemComponent;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = KillDCore, meta = (AllowPrivateAccess = "true"))
		class USnowBall_TPS_KillScoreComp* KillScoreComponent;

	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	bool CanMovePawn = true;
	UPROPERTY(Replicated)
	bool CanAttackPawn = true;

	UPROPERTY(Replicated)
	TArray<USnowBallTPS_StateEffect*> Effects;

	UAnimInstance* AnimAttackInstance;

	bool bIsThatConfuse = false;

	AController* LastDamageInstigator;

	UFUNCTION()
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Interface start
	EPhysicalSurface GetSurfaceType() override;
	TArray<USnowBallTPS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(USnowBallTPS_StateEffect* RemoveEffect) override;
	void AddEffect(USnowBallTPS_StateEffect* NewEffect) override;
	bool CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass) override;
	FName GetNameBoneForStateEffect(FTransform& EmitterTransform);


	//Combat
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Combat")
		float BaseAttackDamage = 30.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
		float RandomRangeAttackDamage = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
		float RadiusMeleeAttack = 30.0f;

	//AI
	UFUNCTION(BlueprintCallable)
		bool CanEnemyAttack();
	UFUNCTION(BlueprintCallable)
		bool CanEnemyMove();
	UFUNCTION(BlueprintCallable)
	virtual	void StartAttackEnemy();



	//Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		FVector SpawnOffsetLocalDamage = FVector(0, 0, 44.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float ConfusionChance = 0.95f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float HitAnimChance = 0.9f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		bool bIsConfusion = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float CoefDamageResist = 1.0f;

	UFUNCTION()
		void HealthChange(float Health, float Damage);
	UFUNCTION(BlueprintNativeEvent)
		void HealthChange_BP(float Health, float Damage);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float DistanceToAttack = 150.0f;

	//Dead
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dead")
		float DropChance = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dead")
		UMaterialInterface* DeathMaterial = nullptr;
	UFUNCTION()
		virtual void CharacterDead();
	UFUNCTION(BlueprintNativeEvent)
		void CharacterDead_BP();
	UFUNCTION()
		void InitDropComponent();

	UFUNCTION()
		void ConfusionHitEnableTimer();
	UFUNCTION()
		void ConfusionHitDisableTimer();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastAnimPlay(UAnimMontage* AnimMontage);

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffect")
		TArray<FMaxCoutStateEffects> MaxCoutStateEffect;

	//Montage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Montage")
		UAnimMontage* DeadMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Montage")
		UAnimMontage* HitMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Montage")
		UAnimMontage* AttackMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Montage")
		UAnimMontage* ConfuseMontage = nullptr;

	//State
	UFUNCTION(BlueprintCallable)
		void SetStateAttack(bool CanAttack);
	UFUNCTION(BlueprintCallable)
		void SetStateMove(bool CanMove);

	//Attack
	UFUNCTION()
	virtual void AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess);
	UFUNCTION()
	virtual	void AttackEnemy(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointNotifyPayload);
	UFUNCTION(NetMulticast, Reliable)
		void StartAttackEnemy_Multicast();


	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxSpeed = 400.0f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticalSystemComponents;

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		USnowBallTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		USnowBallTPS_StateEffect* EffectRemove = nullptr;
	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void SwitchEffect(USnowBallTPS_StateEffect* Effect, bool bIsAdd);
	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ParticleSystem);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ParticleSystem);
};