// Fill out your copyright notice in the Description page of Project Settings.

#include "SnowBall_TPS_EnemyCharacterBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimSequenceBase.h"
#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/PlayerState.h"



// Sets default values
ASnowBall_TPS_EnemyCharacterBase::ASnowBall_TPS_EnemyCharacterBase()
{

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	HealthComponent = CreateDefaultSubobject<USnowBall_TPSHealthComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ASnowBall_TPS_EnemyCharacterBase::CharacterDead);
		HealthComponent->OnHealthChange.AddDynamic(this, &ASnowBall_TPS_EnemyCharacterBase::HealthChange);
	}

	DropItemComponent = CreateDefaultSubobject<USnowBall_TPS_DropItemComp>(TEXT("DropItemComponent"));
	InitDropComponent();

	KillScoreComponent = CreateDefaultSubobject<USnowBall_TPS_KillScoreComp>(TEXT("KillScoreComponent"));
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->MaxWalkSpeed = MaxSpeed;

}

bool ASnowBall_TPS_EnemyCharacterBase::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch,
	FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return Wrote;
}

// Called when the game starts or when spawned
void ASnowBall_TPS_EnemyCharacterBase::BeginPlay()
{
	Super::BeginPlay();

	if (AttackMontage)
	{
		AnimAttackInstance = GetMesh()->GetAnimInstance();

		if (AnimAttackInstance)
		{
			AnimAttackInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &ASnowBall_TPS_EnemyCharacterBase::AttackEnemy);
			AnimAttackInstance->OnMontageEnded.AddDynamic(this, &ASnowBall_TPS_EnemyCharacterBase::AttackAnimEnd);
		}
	}
}

float ASnowBall_TPS_EnemyCharacterBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	if(IsValid(EventInstigator))
		LastDamageInstigator = EventInstigator;

	GetHealthComponent()->ChangeHealthValue_OnServer(DamageAmount * CoefDamageResist * -1);

	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

// Called every frame
void ASnowBall_TPS_EnemyCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnowBall_TPS_EnemyCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

EPhysicalSurface ASnowBall_TPS_EnemyCharacterBase::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;

	if (HealthComponent)
	{
		if (GetMesh())
		{
			UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
			if (myMaterial)
			{
				result = myMaterial->GetPhysicalMaterial()->SurfaceType;
			}
		}
	}
	return result;
}

TArray<USnowBallTPS_StateEffect*> ASnowBall_TPS_EnemyCharacterBase::GetCurrentEffects()
{
	return Effects;
}

void ASnowBall_TPS_EnemyCharacterBase::RemoveEffect(USnowBallTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyPartical)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ASnowBall_TPS_EnemyCharacterBase::AddEffect(USnowBallTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyPartical)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticalEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticalEffect);
		}
	}
	EffectAdd = NewEffect;
}

bool ASnowBall_TPS_EnemyCharacterBase::CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass)
{
	bool result = true;
	int i = 0;
	int32 MaxCoutEffect = 0;
	while (i < MaxCoutStateEffect.Num())
	{
		if (AddEffectClass == MaxCoutStateEffect[i].StateEffect)
		{
			MaxCoutEffect = MaxCoutStateEffect[i].Cout;
		}

		i++;
	}

	i = 0;
	int32 CoutEffect = 0;
	while (i < Effects.Num() && result == true)
	{
		if (Effects[i]->GetClass() == AddEffectClass)
		{
			CoutEffect++;
			if (CoutEffect > MaxCoutEffect)
			{
				result = false;
			}
		}
		i++;
	}

	return result;
}

FName ASnowBall_TPS_EnemyCharacterBase::GetNameBoneForStateEffect(FTransform& EmitterTransform)
{
	FName BoneName = "none";

	if (GetMesh())
	{
		BoneName = GetMesh()->GetBoneName(0);
		EmitterTransform = GetMesh()->GetBoneTransform(0);
	}
	return BoneName;
}

bool ASnowBall_TPS_EnemyCharacterBase::CanEnemyAttack()
{
	return CanAttackPawn;
}

bool ASnowBall_TPS_EnemyCharacterBase::CanEnemyMove()
{
	return CanMovePawn;
}

void ASnowBall_TPS_EnemyCharacterBase::StartAttackEnemy()
{
	SetStateAttack(false);

	StartAttackEnemy_Multicast();
}

void ASnowBall_TPS_EnemyCharacterBase::HealthChange(float Health, float Damage)
{
	if(Health >= 50 && (Health+Damage)<50)
	{
		bIsThatConfuse = true;
		ConfusionHitEnableTimer();
	}
	else
	{
		if(bIsConfusion == false)
		{
			if (UKismetMathLibrary::RandomFloat() > HitAnimChance)
			{
				if (HitMontage)
				{
					ConfusionHitEnableTimer();
				}
			}
		}
	}

	HealthChange_BP(Health, Damage);
}

void ASnowBall_TPS_EnemyCharacterBase::HealthChange_BP_Implementation(float Health, float Damage)
{
}

void ASnowBall_TPS_EnemyCharacterBase::CharacterDead()
{
	if (HasAuthority())
	{
		ASnowBall_TPS_PlayerState* CurrentPlayerState = Cast<ASnowBall_TPS_PlayerState>(LastDamageInstigator->GetPlayerState<APlayerState>());
		if (CurrentPlayerState)
		{
			UE_LOG(LogTemp, Warning, TEXT("ASnowBall_TPS_EnemyCharacterBase::CharacterDead - PlayerState is null"));
		}
		if (CurrentPlayerState)
			CurrentPlayerState->SetScore(CurrentPlayerState->GetScore() + KillScoreComponent->Score);

		if(GetCharacterMovement())
		{
			GetCharacterMovement()->StopMovementImmediately();
		}
		if (IsValid(GetController()))
		{
			GetController()->UnPossess();
		}

		SetLifeSpan(2.0f);
		SetStateMove(false);
		SetStateAttack(false);

		if (IsValid(DropItemComponent))
		{
			DropItemComponent->Drop();
		}

		//GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetMaterial(0, DeathMaterial);

		MulticastAnimPlay(DeadMontage);

		CharacterDead_BP();
	}
	else
	{
		GetMesh()->SetMaterial(0, DeathMaterial);
	}
}

void ASnowBall_TPS_EnemyCharacterBase::CharacterDead_BP_Implementation()
{
}

void ASnowBall_TPS_EnemyCharacterBase::InitDropComponent()
{
	if (HasAuthority())
	{
		if (UKismetMathLibrary::RandomFloat() > DropChance)
		{
			switch (UKismetMathLibrary::RandomIntegerInRange(0, 4))
			{
			case 0:
				DropItemComponent->WeaponType = EWeaponType::GrenadeType;
				DropItemComponent->Cout = 3;
				break;
			case 1:
				DropItemComponent->WeaponType = EWeaponType::GunType;
				DropItemComponent->Cout = 12;
				break;
			case 2:
				DropItemComponent->WeaponType = EWeaponType::PistolType;
				DropItemComponent->Cout = 20;
				break;
			case 3:
				DropItemComponent->WeaponType = EWeaponType::RifleType;
				DropItemComponent->Cout = 50;
				break;
			case 4:
				DropItemComponent->WeaponType = EWeaponType::TraceType;
				DropItemComponent->Cout = 100;
				break;
			default:
				break;
			}
		}
	}
	else
	{
		DropItemComponent->DestroyComponent();
	}
}

void ASnowBall_TPS_EnemyCharacterBase::ConfusionHitEnableTimer()
{
	if (HasAuthority())
	{
		GetMesh()->GetAnimInstance()->StopAllMontages(0.0f);

		if (bIsThatConfuse)
		{
			MaxSpeed = GetCharacterMovement()->MaxWalkSpeed;

			SetStateMove(false);
			GetCharacterMovement()->MaxWalkSpeed = 0;
		}

		SetStateAttack(false);
		bIsConfusion = true;

		UKismetSystemLibrary::K2_SetTimer(this, "ConfusionHitDisableTimer", ConfuseMontage->GetPlayLength(), false);

		if (bIsThatConfuse)
			MulticastAnimPlay(ConfuseMontage);
		else
			MulticastAnimPlay(HitMontage);
	}
}

void ASnowBall_TPS_EnemyCharacterBase::ConfusionHitDisableTimer()
{
	if (bIsThatConfuse)
	{
		SetStateMove(true);
		GetCharacterMovement()->MaxWalkSpeed = MaxSpeed;
		bIsThatConfuse = false;
	}

	SetStateAttack(true);

	bIsConfusion = false;
}

void ASnowBall_TPS_EnemyCharacterBase::MulticastAnimPlay_Implementation(UAnimMontage* AnimMontage)
{
	PlayAnimMontage(AnimMontage);
}

void ASnowBall_TPS_EnemyCharacterBase::SetStateAttack(bool CanAttack)
{
	CanAttackPawn = CanAttack;
}

void ASnowBall_TPS_EnemyCharacterBase::SetStateMove(bool CanMove)
{
	CanMovePawn = CanMove;
}

void ASnowBall_TPS_EnemyCharacterBase::AttackAnimEnd(UAnimMontage* Montage, bool bIsSuccess)
{
	SetStateAttack(true);
}

void ASnowBall_TPS_EnemyCharacterBase::AttackEnemy(FName NotifyName,
                                                   const FBranchingPointNotifyPayload& BranchingPointNotifyPayload)
{
	if (NotifyName == "Attack")
	{
		TArray<AActor*> ActorIgnore;
		FVector StartTraceVector = GetActorLocation() + GetActorForwardVector() * 50.0f;
		FVector EndTraceVector = GetActorLocation() + GetActorForwardVector() * 200.0f;
		FHitResult Hit;
		UKismetSystemLibrary::SphereTraceSingle(this, StartTraceVector, EndTraceVector, RadiusMeleeAttack,
			ETraceTypeQuery::TraceTypeQuery14, false, ActorIgnore, EDrawDebugTrace::ForDuration,
			Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

		if (Hit.Actor.IsValid())
		{
			if (Hit.Actor->ActorHasTag("Player"))
			{
				const TSubclassOf<UDamageType> DamageType;
				const float Damage = UKismetMathLibrary::RandomFloatInRange(BaseAttackDamage - RandomRangeAttackDamage, BaseAttackDamage + RandomRangeAttackDamage);

				UGameplayStatics::ApplyDamage(Hit.GetActor(), Damage, GetController(), this,DamageType);
			}
		}
	}
}

void ASnowBall_TPS_EnemyCharacterBase::StartAttackEnemy_Multicast_Implementation()
{
	if (HasAuthority())
	{
		if (AttackMontage)
		{
			if (AnimAttackInstance)
			{
				AnimAttackInstance->Montage_Play(AttackMontage);
			}
		}
	}
	else
	{
		if (AttackMontage)
			PlayAnimMontage(AttackMontage);
	}
}

void ASnowBall_TPS_EnemyCharacterBase::EffectAdd_OnRep()
{
	if (EffectAdd)
		SwitchEffect(EffectAdd, true);
}

void ASnowBall_TPS_EnemyCharacterBase::EffectRemove_OnRep()
{
	if (EffectRemove)
		SwitchEffect(EffectRemove, false);
}

void ASnowBall_TPS_EnemyCharacterBase::SwitchEffect(USnowBallTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticalEffect)
		{
			FName NameBoneToAttach = Effect->NameBone;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UParticleSystemComponent* myParticalSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticalEffect, myMesh, NameBoneToAttach,
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticalSystemComponents.Add(myParticalSystem);
			}
		}
	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		if (ParticalSystemComponents.Num() > 0)
		{
			while (i < ParticalSystemComponents.Num(), !bIsFind)
			{
				if (ParticalSystemComponents[i]->Template && Effect->ParticalEffect && ParticalSystemComponents[i]->Template == Effect->ParticalEffect)
				{
					ParticalSystemComponents[i]->DeactivateSystem();
					ParticalSystemComponents[i]->DestroyComponent();
					ParticalSystemComponents.RemoveAt(i);

					bIsFind = true;
				}
			}
		}
	}
}

void ASnowBall_TPS_EnemyCharacterBase::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ParticleSystem)
{
	UTypes::ExecuteEffectAdded(ParticleSystem, this, FVector(0), FName("spine_01"));
}

void ASnowBall_TPS_EnemyCharacterBase::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ParticleSystem)
{
	ExecuteEffectAdded_Multicast(ParticleSystem);
}

void ASnowBall_TPS_EnemyCharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASnowBall_TPS_EnemyCharacterBase, Effects);
	DOREPLIFETIME(ASnowBall_TPS_EnemyCharacterBase, EffectAdd);
	DOREPLIFETIME(ASnowBall_TPS_EnemyCharacterBase, EffectRemove);
	DOREPLIFETIME(ASnowBall_TPS_EnemyCharacterBase, CanMovePawn);
	DOREPLIFETIME(ASnowBall_TPS_EnemyCharacterBase, CanAttackPawn);
}
