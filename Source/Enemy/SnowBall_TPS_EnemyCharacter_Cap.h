// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemy/SnowBall_TPS_EnemyCharacterBase.h"
#include "Weapon/Weapon/WeaponDefault.h"
#include "SnowBall_TPS_EnemyCharacter_Cap.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_EnemyCharacter_Cap : public ASnowBall_TPS_EnemyCharacterBase
{
	GENERATED_BODY()

protected:

public:

	//Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Current weapon")
		AWeaponDefault* CurrentWeapon = nullptr;

};
