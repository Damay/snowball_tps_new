// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "FuncLibrary/Types.h"
#include "SnowBallTPS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USnowBallTPS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SNOWBALL_TPS_API ISnowBallTPS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
	//void AviableForEffectBP();
	//UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
	//bool AviableEffect();

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<USnowBallTPS_StateEffect*> GetCurrentEffects();
	virtual void RemoveEffect(USnowBallTPS_StateEffect* removeEffect);
	virtual void AddEffect(USnowBallTPS_StateEffect* newEffect);
	virtual bool CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass);
	virtual FName GetNameBoneForStateEffect(FTransform &EmitterTransform);
	virtual void DropWeaponToWorld(FDropItem DropItemInfo);

};
