// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBallTPS_IGameActor.h"

// Add default functionality here for any ISnowBallTPS_IGameActor functions that are not pure virtual.

EPhysicalSurface ISnowBallTPS_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<USnowBallTPS_StateEffect*> ISnowBallTPS_IGameActor::GetCurrentEffects()
{
	TArray<USnowBallTPS_StateEffect*> Effect;
	return Effect;
}

void ISnowBallTPS_IGameActor::RemoveEffect(USnowBallTPS_StateEffect* removeEffect)
{

}

void ISnowBallTPS_IGameActor::AddEffect(USnowBallTPS_StateEffect* newEffect)
{

}

bool ISnowBallTPS_IGameActor::CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass)
{
	return true;
}

FName ISnowBallTPS_IGameActor::GetNameBoneForStateEffect(FTransform& LocationOffset)
{
	return "none";
}

void ISnowBallTPS_IGameActor::DropWeaponToWorld(FDropItem DropItemInfo)
{
}


