// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBallTPS_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "Net/UnrealNetwork.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASnowBallTPS_EnvironmentStructure::ASnowBallTPS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<USnowBall_TPSHealthComponent>(TEXT("HealthComponent"));

	SetReplicates(true);
}

bool ASnowBallTPS_EnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch,
	FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return Wrote;
}

// Called when the game starts or when spawned
void ASnowBallTPS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ASnowBallTPS_EnvironmentStructure::Dead);
	}
}

float ASnowBallTPS_EnvironmentStructure::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	GetHealthComponent()->ChangeHealthValue_OnServer(DamageAmount * CoefDamageResist * -1);

	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

// Called every frame
void ASnowBallTPS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ASnowBallTPS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));

	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return result;
}

TArray<USnowBallTPS_StateEffect*> ASnowBallTPS_EnvironmentStructure::GetCurrentEffects()
{
	return Effects;
}

void ASnowBallTPS_EnvironmentStructure::RemoveEffect(USnowBallTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyPartical)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ASnowBallTPS_EnvironmentStructure::AddEffect(USnowBallTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyPartical)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticalEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticalEffect);
		}
	}
}

bool ASnowBallTPS_EnvironmentStructure::CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass)
{
	bool result = true;
	int i = 0;
	int32 MaxCoutEffect = 0;
	while (i < MaxCoutStateEffect.Num())
	{
		if (AddEffectClass == MaxCoutStateEffect[i].StateEffect)
		{
			MaxCoutEffect = MaxCoutStateEffect[i].Cout;
		}

		i++;
	}
	
	i = 0;
	int32 CoutEffect = 0;
	while (i < Effects.Num() && result == true)
	{
		if (Effects[i]->GetClass() == AddEffectClass)
		{
			CoutEffect++;
			if (CoutEffect >= MaxCoutEffect)
			{
				result = false;
			}
		}
		i++;
	}

	return result;
}

FName ASnowBallTPS_EnvironmentStructure::GetNameBoneForStateEffect(FTransform& EmitterTransform)
{
	EmitterTransform = this->GetActorTransform();

	return "none";
}


void ASnowBallTPS_EnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ParticleSystem)
{
	UTypes::ExecuteEffectAdded(ParticleSystem, this, OffsetEffect, NAME_None);
}

void ASnowBallTPS_EnvironmentStructure::Dead()
{
	int i = 0;
	while (i < Effects.Num())
	{
		if (Effects[i])
		{
			Effects[i]->DestroyObject();
		}
	}
	
	Dead_BP();
}

void ASnowBallTPS_EnvironmentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
		SwitchEffect(EffectAdd, true);
}

void ASnowBallTPS_EnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
		SwitchEffect(EffectRemove, false);
}

void ASnowBallTPS_EnvironmentStructure::SwitchEffect(USnowBallTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticalEffect)
		{
			FName NameBoneToAttach = NAME_None;
			FVector Loc = OffsetEffect;

			USceneComponent* myScene = GetRootComponent();
			if (myScene)
			{
				UParticleSystemComponent* myParticalSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticalEffect, myScene, NameBoneToAttach,
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticalSystemComponents.Add(myParticalSystem);
			}
		}
	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		if (ParticalSystemComponents.Num() > 0)
		{
			while (i < ParticalSystemComponents.Num(), !bIsFind)
			{
				if (ParticalSystemComponents[i]->Template && Effect->ParticalEffect && ParticalSystemComponents[i]->Template == Effect->ParticalEffect)
				{
					ParticalSystemComponents[i]->DeactivateSystem();
					ParticalSystemComponents[i]->DestroyComponent();
					ParticalSystemComponents.RemoveAt(i);

					bIsFind = true;
				}
			}
		}
	}
}

void ASnowBallTPS_EnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ParticleSystem)
{
	ExecuteEffectAdded_Multicast(ParticleSystem);
}


void ASnowBallTPS_EnvironmentStructure::Dead_BP_Implementation()
{

}

void ASnowBallTPS_EnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASnowBallTPS_EnvironmentStructure, Effects);
	DOREPLIFETIME(ASnowBallTPS_EnvironmentStructure, EffectAdd);
	DOREPLIFETIME(ASnowBallTPS_EnvironmentStructure, EffectRemove);
}