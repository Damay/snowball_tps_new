// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "FuncLibrary/Types.h"
#include "Component/SnowBall_TPSHealthComponent.h"
#include "SnowBallTPS_EnvironmentStructure.generated.h"

UCLASS()
class SNOWBALL_TPS_API ASnowBallTPS_EnvironmentStructure : public AActor, public ISnowBallTPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnowBallTPS_EnvironmentStructure();

	FORCEINLINE class USnowBall_TPSHealthComponent* GetHealthComponent() const { return HealthComponent; }


protected:

	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USnowBall_TPSHealthComponent* HealthComponent;

	UFUNCTION()
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float CoefDamageResist = 1.0f;

	EPhysicalSurface GetSurfaceType() override;

	TArray<USnowBallTPS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(USnowBallTPS_StateEffect* RemoveEffect) override;
	void AddEffect(USnowBallTPS_StateEffect* NewEffect) override;
	bool CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass) override;
	FName GetNameBoneForStateEffect(FTransform& EmitterTransform) override;

	//Effect
	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
	TArray<USnowBallTPS_StateEffect*> Effects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffect")
	TArray<FMaxCoutStateEffects> MaxCoutStateEffect;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		USnowBallTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		USnowBallTPS_StateEffect* EffectRemove = nullptr;
	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void SwitchEffect(USnowBallTPS_StateEffect* Effect, bool bIsAdd);
	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ParticleSystem);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ParticleSystem);

	UFUNCTION()
	void Dead();
	UFUNCTION(BlueprintNativeEvent)
	void Dead_BP();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticalSystemComponents;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		FVector OffsetEffect = FVector(0);
};
