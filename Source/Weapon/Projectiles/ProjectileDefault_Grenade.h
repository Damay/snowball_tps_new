// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	
private:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;
	virtual void InitProjectile(FProjectileInfo InitParam) override;

	void Explose();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade")
	bool TimerEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
		float TimeToExplose = 5.0f;

	float TimerToExplose = 0.0f;

	UFUNCTION(NetMulticast, Reliable)
		void SoundFxGrenadeExplose_Multicast(UParticleSystem* ParticleGrenadeExplose, USoundBase* SoundGrenadeExplose);
};
