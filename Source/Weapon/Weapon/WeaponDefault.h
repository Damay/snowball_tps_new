// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "FuncLibrary/Types.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSpent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimMontage);

UCLASS()
class SNOWBALL_TPS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponFireStart OnWeaponFireStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShellDropLocation = nullptr;

	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo WeaponInfo;

//	FORCEINLINE class UParticleSystemComponent* GetUParticleSystemComponent() const { return LazerFx; }
//
//private:
//
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Lazer, meta = (AllowPrivateAccess = "true"))
		//class UParticleSystemComponent* LazerFx;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);

	UParticleSystemComponent* Lazer = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lazer")
	UParticleSystem* LazerFx = nullptr;

	void WeaponInit();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Reloading")
	bool WeaponReloading = false;

	UFUNCTION(Server, Reliable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	UFUNCTION(BlueprintCallable)
	bool CheckWeaponCanFire();

	void Fire();

	//ShotLocation
	void FindEndLocation();
	UPROPERTY(Replicated)
	FVector ShotEndLocation = FVector(0.0f);
	float SizeChanelToChangeShootDirectionLogic = 120.0f;

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	float CoefDispersion = 0.0f;

	// Timers flag
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reloading")
	float ReloadTimer = 0.0f;
	//Remove
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reloading")
	float ReloadTime = 0.0f;

	//Shoot direction
	class ASnowBall_TPSCharacter* myPawn = nullptr;
	bool bIsPalyer = false;

	//Block fire when the character rolls
	bool bIsRoll = false;

	bool WeaponAiming;

	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
			float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	//DropShellBullet
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;
	void MagazineDropTick(float DelataTime);

	//DropMagazine
	bool DropMagazineFlag = false;
	float DropMagazineTimer = -1.0f;
	void ShellBulletDropTick(float DelataTime);

	//Confusion
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	int Confusion = 2;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	FProjectileInfo GetProjectile();

	void StartReload();
	void EndReload();

	uint8 GetNumberProjectileByShot() const;

	//CancelReload
	UFUNCTION(BlueprintCallable)
	void CancelReload();
	bool MagazineIsDroped = false;

	bool CheckWeaponReload();
	int8 GetAviableAmmoForReload();

	UFUNCTION(NetMulticast, Reliable)
		void AnimWeaponFire_Multicast(UAnimMontage* AnimFireMontage);
	UFUNCTION(NetMulticast, Reliable)
		void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);
	UFUNCTION(NetMulticast, Reliable)
		void SoundFxWeaponFire_Multicast(UParticleSystem* ParticleWeaponFire, USoundBase* SoundWeaponFire);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnLazer_Multicast();
};
