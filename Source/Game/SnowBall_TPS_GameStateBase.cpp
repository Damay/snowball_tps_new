// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/SnowBall_TPS_GameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "Game/SnowBall_TPSGameMode.h"
#include "Net/UnrealNetwork.h"

void ASnowBall_TPS_GameStateBase::SetCurrentPhase(int32 NewPhase)
{
	CurrentPhase = NewPhase;
}

void ASnowBall_TPS_GameStateBase::SetLevelSettings(int32 ValueCheckPointToBoss, int32 ValueScoreToBoss, TArray<int32> ValuePhaseEnemyMaxCout, int32 ValueCoutCheckPointToBoss, UClass* ValueLevelBoss)
{
	PhaseEnemyMaxCout = ValuePhaseEnemyMaxCout;
	ScoreToBoss = ValueScoreToBoss;
	CheckPointToBoss = ValueCheckPointToBoss;
	CoutCheckPointToBoss = ValueCoutCheckPointToBoss;
	LevelBoss = ValueLevelBoss;
}

int32 ASnowBall_TPS_GameStateBase::GetCurrentPhase()
{
	return CurrentPhase;
}

bool ASnowBall_TPS_GameStateBase::CheckEnemyForRespawn()
{
	bool bIsSuccess = false;

	if (PhaseEnemyMaxCout.IsValidIndex(CurrentPhase))
	{
		if (PhaseEnemyMaxCout[CurrentPhase] > CurrentCoutEnemy)
		{
			bIsSuccess = true;
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ASnowBall_TPS_GameStateBase::CheckEnemyForRespawn - CurrentPhase isn't find"));

		return bIsSuccess;
}


void ASnowBall_TPS_GameStateBase::IncrementCheckPoint()
{
	CurrentCheckPointForBoss++;
}

void ASnowBall_TPS_GameStateBase::IncrementScore(int32 ChangeValue)
{
	Score += ChangeValue;

	IncrementEnemy(-1);

	IncrementScore_BP();

	if (Score >= ScoreToBoss)
	{
		ASnowBall_TPSGameMode* GameMode = Cast<ASnowBall_TPSGameMode>(UGameplayStatics::GetGameMode(this));

		if(GameMode)
		{
			if (bIsBossDead || !IsValid(LevelBoss))
			{
				GameMode->EndGameGameMode(true);
			}
			else
			{
				if (!bIsBossAppeared && CurrentCheckPointForBoss >= CoutCheckPointToBoss)
				{
					GameMode->TrySpawnBoss(LevelBoss);
					bIsBossAppeared = true;
				}
			}
		}

	}
}

void ASnowBall_TPS_GameStateBase::IncrementScore_BP_Implementation()
{

}

void ASnowBall_TPS_GameStateBase::IncrementEnemy(int32 ChangeValue)
{
	CurrentCoutEnemy += ChangeValue;

	IncrementEnemy_BP();
}

void ASnowBall_TPS_GameStateBase::IncrementEnemy_BP_Implementation()
{

}

void ASnowBall_TPS_GameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASnowBall_TPS_GameStateBase, CurrentPhase);
	DOREPLIFETIME(ASnowBall_TPS_GameStateBase, Score);
	DOREPLIFETIME(ASnowBall_TPS_GameStateBase, CurrentCoutEnemy);
}