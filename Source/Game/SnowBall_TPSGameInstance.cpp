// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/SnowBall_TPSGameInstance.h"

bool USnowBall_TPSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;

	if (WeaponInfoTable)
	{	
		FWeaponInfo* WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);

		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("USnowBall_TPSGameInstance::GetWeaponInfoByName - WeaponTable is NULL"))
	}

	return bIsFind;
}

bool USnowBall_TPSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		TArray<FName>RowName = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		while (i < RowName.Num() && !bIsFind)
		{
			const FDropItem* DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowName[i], "");

			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = (*DropItemInfoRow);
				bIsFind = true;
			}
			
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("USnowBall_TPSGameInstance::GetDropItemInfoByName - DropItemInfoTable is NULL"))
	}

	return bIsFind;
}

bool USnowBall_TPSGameInstance::GetEnemyInfoByName(FName NameEnemy, FEnemyCharacters& Enemy)
{
	bool bIsFind = false;

	if (EnemyInfoTable)
	{
		FEnemyCharacters* EnemyInfoRow = EnemyInfoTable->FindRow<FEnemyCharacters>(NameEnemy, "", false);

		if (EnemyInfoRow)
		{
			bIsFind = true;
			Enemy = *EnemyInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("USnowBall_TPSGameInstance::GetWeaponInfoByName - WeaponTable is NULL"))
	}

	return bIsFind;
}
