// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/SnowBall_TPS_PlayerSpectator.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"

ASnowBall_TPS_PlayerSpectator::ASnowBall_TPS_PlayerSpectator()
{
	bAddDefaultMovementBindings = false;

	// Create a camera boom...
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	SpringArm->TargetArmLength = 700.f;
	SpringArm->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	SpringArm->bDoCollisionTest = false;

	// Create a camera...
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

void ASnowBall_TPS_PlayerSpectator::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(IsValid(TargetActor))
	{
		SetActorLocation(TargetActor->GetActorLocation());
	}
}

void ASnowBall_TPS_PlayerSpectator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASnowBall_TPS_PlayerSpectator, TargetActor);
}