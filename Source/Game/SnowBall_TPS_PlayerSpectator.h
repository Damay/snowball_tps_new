// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "SnowBall_TPS_PlayerSpectator.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_PlayerSpectator : public ASpectatorPawn
{
	GENERATED_BODY()

public:
	ASnowBall_TPS_PlayerSpectator();

	/** Returns Camera subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return Camera; }
	/** Returns SpringArm subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return SpringArm; }

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* SpringArm;

public:

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Target")
		AActor* TargetActor = nullptr;

	virtual void Tick(float DeltaSeconds) override;
};
