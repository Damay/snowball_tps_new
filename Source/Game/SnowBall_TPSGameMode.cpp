// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnowBall_TPSGameMode.h"
#include "SnowBall_TPSPlayerController.h"
#include "Character/SnowBall_TPSCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Game/SnowBall_TPS_GameStateBase.h"
#include "Enemy/SnowBall_TPS_EnemyCharacterBase.h"
#include "Game/SnowBall_TPS_PlayerSpectator.h"
#include "Game/SnowBall_TPSGameInstance.h"


ASnowBall_TPSGameMode::ASnowBall_TPSGameMode()
{
	// use our custom PlayerController class
	//PlayerControllerClass = ASnowBall_TPSPlayerController::StaticClass();

	//// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	//if (PlayerPawnBPClass.Class != nullptr)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}


}

void ASnowBall_TPSGameMode::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_RespawnEnemyTimer, this, &ASnowBall_TPSGameMode::TryRespawnEnemy, TimeToRespawnEnemy, true);
}

FString ASnowBall_TPSGameMode::FindPhaseName(int32 CurrentPhase)
{
	FString ReturnString = "";

	switch (CurrentPhase)
	{
	case 0:
		ReturnString = "Phase0";
		break;
	case 1:
		ReturnString = "Phase1";
		break;
	case 2:
		ReturnString = "Phase2";
		break;
	default:
		break;
	}

	return ReturnString;
}

TArray<ASnowBall_TPS_EnemySpawnBase*> ASnowBall_TPSGameMode::FindPhaseSpawnEnemyBase(int32 CurrentPhase)
{
	TArray<ASnowBall_TPS_EnemySpawnBase*> ReturnArray;
	switch (CurrentPhase)
	{
	case 0:
		ReturnArray = SpawnBasePhase0;
		break;
	case 1:
		ReturnArray = SpawnBasePhase1;
		break;
	case 2:
		ReturnArray = SpawnBasePhase2;
		break;
	default:
		break;
	}

	return ReturnArray;
}

void ASnowBall_TPSGameMode::TryToRespawnAtSpectator(ASnowBall_TPSPlayerController* DeadController)
{
	ASnowBall_TPSCharacter* TargetForSpectator = nullptr;

	for (int i = 0; i < Controllers.Num(); ++i)
	{
		auto ControllerPawn = Cast<ASnowBall_TPSCharacter>(Controllers[i]->GetPawn());

		if (ControllerPawn && ControllerPawn->GetCharHealthComponent()->GetIsAlive())
		{
			TargetForSpectator = ControllerPawn;
			break;
		}
	}

	if (IsValid(TargetForSpectator))
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FVector SpawnLocation = TargetForSpectator->GetActorLocation();
		FRotator SpawnRotation = TargetForSpectator->GetActorRotation();

		auto Spectator = Cast<ASnowBall_TPS_PlayerSpectator>(GetWorld()->SpawnActor(ASnowBall_TPS_PlayerSpectator::StaticClass(), &SpawnLocation, &SpawnRotation, SpawnParams));
		DeadController->Possess(Spectator);
		Spectator->TargetActor = TargetForSpectator;
	}
	else
	{
		EndGameGameMode(false);
	}
}

void ASnowBall_TPSGameMode::TryRespawnPlayer(ASnowBall_TPSPlayerController* DeadController)
{
	ASnowBall_TPS_GameStateBase* GameStateBase = Cast<ASnowBall_TPS_GameStateBase>(UGameplayStatics::GetGameState(this));

	int32 CurrentPhase = GameStateBase->GetCurrentPhase();

	AActor* RespawnSpot = K2_FindPlayerStart(DeadController, FindPhaseName(CurrentPhase));
	RestartPlayerAtPlayerStart(DeadController, RespawnSpot);

}

void ASnowBall_TPSGameMode::TryRespawnEnemy()
{
	ASnowBall_TPS_GameStateBase* GameStateBase = Cast<ASnowBall_TPS_GameStateBase>(UGameplayStatics::GetGameState(this));
	if (GameStateBase)
	{
		if(GameStateBase->CheckEnemyForRespawn())
		{
			ASnowBall_TPS_EnemySpawnBase* CurrentBase;

			int32 NumCurrentBase = UKismetMathLibrary::RandomIntegerInRange(0, FindPhaseSpawnEnemyBase(GameStateBase->CurrentPhase).Num() - 1);

			CurrentBase = FindPhaseSpawnEnemyBase(GameStateBase->CurrentPhase)[NumCurrentBase];

			FVector SpawnLocation;
			FRotator SpawnRotator = FRotator(0);
			FActorSpawnParameters SpawmParams;
			SpawmParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			if (CurrentBase->bIsPoint)
			{
				SpawnLocation = CurrentBase->GetActorLocation();
			}
			else
			{
				SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(CurrentBase->GetActorLocation(), CurrentBase->GetBoxComponent()->GetScaledBoxExtent());
			}

			FEnemyCharacters Enemy;

			USnowBall_TPSGameInstance* GameInstance = Cast<USnowBall_TPSGameInstance>(GetGameInstance());
			if (GameInstance)
			{
				FName EnemyName = "Simple";
				if(UKismetMathLibrary::RandomFloatInRange(0, 1)>0.8)
				{
					EnemyName = "Lich";
				}
				if (GameInstance->GetEnemyInfoByName(EnemyName, Enemy))
				{
					GetWorld()->SpawnActor(Enemy.Enemy, &SpawnLocation, &SpawnRotator, SpawmParams);
				}
			}

			GameStateBase->IncrementEnemy(1);
		}
	}
}

void ASnowBall_TPSGameMode::TrySpawnBoss(UClass* LevelBoss)
{
	FVector SpawnLocation = SpawnBoss->GetActorLocation();
	FRotator SpawnRotator = FRotator(0);
	FActorSpawnParameters SpawmParams;
	SpawmParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	GetWorld()->SpawnActor(LevelBoss, &SpawnLocation, &SpawnRotator, SpawmParams);
}

void ASnowBall_TPSGameMode::NextPhaseStart()
{
	TArray<ASnowBall_TPS_EnemyCharacterBase*> AllEnemyOnOldPhase;

	int i = 0;
	while (i<AllEnemyOnOldPhase.Num())
	{
		if (AllEnemyOnOldPhase[i])
		{
			if (AllEnemyOnOldPhase[i]->GetHealthComponent()->GetIsAlive())
			{
				if (AllEnemyOnOldPhase[i]->GetKillScoreComponent())
				{
					AllEnemyOnOldPhase[i]->GetKillScoreComponent()->DestroyComponent();
				}
			}
		}
	}
}

void ASnowBall_TPSGameMode::EnemyDead(int32 Score)
{
	if(Score > 0)
	{
		ASnowBall_TPS_GameStateBase* myGameStateBase = Cast<ASnowBall_TPS_GameStateBase>(UGameplayStatics::GetGameState(this));
		if (myGameStateBase)
		{
			myGameStateBase->IncrementScore(Score);
		}
	}
}

void ASnowBall_TPSGameMode::EndGameGameMode(bool bIsWin)
{
	for (int i = 0; i < Controllers.Num(); ++i)
	{
		ASnowBall_TPSPlayerController* PlayerController = Cast<ASnowBall_TPSPlayerController>(Controllers[i]);
		PlayerController->EndGamePlayerController(bIsWin);
		UE_LOG(LogTemp, Warning, TEXT("EndGame"));
	}
}
