// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SnowBall_TPSPlayerController.generated.h"

UCLASS()
class ASnowBall_TPSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ASnowBall_TPSPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	int32 CurrentCoutLife = 3;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();



public:

	void CharDead();

	bool CheckPlayerRespawn();

	UFUNCTION(BlueprintNativeEvent)
		void IncrementLife_BP(int32 CoutLife);

	UFUNCTION(Client, Reliable)
		void EndGamePlayerController(bool bIsWin);

	UFUNCTION(BlueprintNativeEvent)
		void EndGamePlayerController_BP(bool bIsWin);
};


