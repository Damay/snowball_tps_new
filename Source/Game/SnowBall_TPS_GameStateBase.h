// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Enemy/SnowBall_TPS_EnemyCharacter_Boss.h"
#include "SnowBall_TPS_GameStateBase.generated.h"


/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_GameStateBase : public AGameStateBase
{
	GENERATED_BODY()

protected:

	//LevelSetting
	TArray<int32> PhaseEnemyMaxCout;
	int32 ScoreToBoss = 0;
	int32 CheckPointToBoss = 0;
	int32 CoutCheckPointToBoss = 0;


public:

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category="Stats")
		int32 Score = 0;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 CurrentPhase = 0;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 CurrentCoutEnemy = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 CurrentCheckPointForBoss = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
		UClass* LevelBoss = nullptr;

	bool bIsBossAppeared = false;
	bool bIsBossDead = false;

	UFUNCTION(BlueprintCallable)
		void SetCurrentPhase(int32 NewPhase);
	UFUNCTION(BlueprintCallable)
		void SetLevelSettings(int32 ValueCheckPointToBoss, int32 ValueScoreToBoss, TArray<int32> ValuePhaseEnemyMaxCout, int32 ValueCoutCheckPointToBoss, UClass* ValueLevelBoss);

	//Check
	UFUNCTION(BlueprintCallable)
		int32 GetCurrentPhase();
	UFUNCTION(BlueprintCallable)
		bool CheckEnemyForRespawn();

	//Increment
	UFUNCTION(BlueprintCallable)
		void IncrementCheckPoint();
	UFUNCTION(BlueprintCallable)
		void IncrementScore(int32 ChangeValue);
	UFUNCTION(BlueprintNativeEvent)
		void IncrementScore_BP();
	UFUNCTION(BlueprintCallable)
		void IncrementEnemy(int32 ChangeValue);
	UFUNCTION(BlueprintNativeEvent)
		void IncrementEnemy_BP();
};
