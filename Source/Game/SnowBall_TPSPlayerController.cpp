// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnowBall_TPSPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Character/SnowBall_TPSCharacter.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "SnowBall_TPSGameMode.h"

ASnowBall_TPSPlayerController::ASnowBall_TPSPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ASnowBall_TPSPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void ASnowBall_TPSPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ASnowBall_TPSPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ASnowBall_TPSPlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ASnowBall_TPSPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ASnowBall_TPSPlayerController::MoveToTouchLocation);

	InputComponent->BindAction("ResetVR", IE_Pressed, this, &ASnowBall_TPSPlayerController::OnResetVR);
}

void ASnowBall_TPSPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ASnowBall_TPSPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (ASnowBall_TPSCharacter* MyPawn = Cast<ASnowBall_TPSCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void ASnowBall_TPSPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ASnowBall_TPSPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ASnowBall_TPSPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ASnowBall_TPSPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void ASnowBall_TPSPlayerController::CharDead()
{
	ASnowBall_TPSGameMode* GameMode = Cast<ASnowBall_TPSGameMode>(UGameplayStatics::GetGameMode(this));

	if (CheckPlayerRespawn())
		GameMode->TryRespawnPlayer(this);
	else
		GameMode->TryToRespawnAtSpectator(this);
}

bool ASnowBall_TPSPlayerController::CheckPlayerRespawn()
{
	CurrentCoutLife--;

	IncrementLife_BP(CurrentCoutLife);

	bool bIsSuccess = false;
	if (CurrentCoutLife >= 1)
	{
		bIsSuccess = true;
	}

	return bIsSuccess;
}

void ASnowBall_TPSPlayerController::IncrementLife_BP_Implementation(int32 CoutLife)
{
}

void ASnowBall_TPSPlayerController::EndGamePlayerController_Implementation(bool bIsWin)
{
	EndGamePlayerController_BP(bIsWin);
}


void ASnowBall_TPSPlayerController::EndGamePlayerController_BP_Implementation(bool bIsWin)
{
}