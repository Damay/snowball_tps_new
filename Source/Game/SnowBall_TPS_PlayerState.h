// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FuncLibrary/Types.h"
#include "SnowBall_TPS_PlayerState.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_PlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	//State
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		TArray<FWeaponSlot> WeaponSlot = { {"Rifle", 50} };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		TArray<FAmmoSlot> AmmoSlot = {{EWeaponType::RifleType, 0, 150}, {EWeaponType::GunType, 0, 36}, {EWeaponType::GrenadeType, 0, 9}, {EWeaponType::PistolType, 0, 60}, {EWeaponType::TraceType, 0, 300}};

	UFUNCTION(BlueprintCallable)
		void SaveCurrentInventory(TArray<FWeaponSlot> CurrentWeaponSlot, TArray<FAmmoSlot> CurrentAmmoSlot);
};
