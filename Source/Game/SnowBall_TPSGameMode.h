// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Enemy/SnowBall_TPS_EnemySpawnBase.h"
#include "SnowBall_TPSPlayerController.h"
#include "SnowBall_TPSGameMode.generated.h"

UCLASS(minimalapi)
class ASnowBall_TPSGameMode : public AGameModeBase
{
	GENERATED_BODY()


public:

	ASnowBall_TPSGameMode();

protected:

	// Called when the game starts or when spawned 
	virtual void BeginPlay() override;

	//Find param
	FString FindPhaseName(int32 CurrentPhase);
	TArray<ASnowBall_TPS_EnemySpawnBase*> FindPhaseSpawnEnemyBase(int32 CurrentPhase);

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller")
		TArray<AController*> Controllers;

	//Timer
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Timer")
		float TimeToRespawnEnemy = 1.0f;
	FTimerHandle TimerHandle_RespawnEnemyTimer;

	//TryRespawnFunction
	UFUNCTION(BlueprintCallable)
		void TryRespawnPlayer(ASnowBall_TPSPlayerController* DeadController);
	UFUNCTION()
		void TryRespawnEnemy();
	UFUNCTION()
		void TrySpawnBoss(UClass* LevelBoss);
	UFUNCTION()
		void TryToRespawnAtSpectator(ASnowBall_TPSPlayerController* DeadController);

	UFUNCTION(BlueprintCallable)
		void NextPhaseStart();

	//Increment
	UFUNCTION(BlueprintCallable)
		void EnemyDead(int32 Score);

	//EnemySpawnBase
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		TArray<ASnowBall_TPS_EnemySpawnBase*> SpawnBasePhase0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		TArray<ASnowBall_TPS_EnemySpawnBase*> SpawnBasePhase1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		TArray<ASnowBall_TPS_EnemySpawnBase*> SpawnBasePhase2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		ASnowBall_TPS_EnemySpawnBase* SpawnBoss;

	//EndGameGameMode
	UFUNCTION(BlueprintCallable)
		void EndGameGameMode(bool bIsWin);
};



