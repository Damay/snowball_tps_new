// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/SnowBall_TPS_PlayerState.h"

void ASnowBall_TPS_PlayerState::SaveCurrentInventory(TArray<FWeaponSlot> CurrentWeaponSlot, TArray<FAmmoSlot> CurrentAmmoSlot)
{
	WeaponSlot = CurrentWeaponSlot;
	AmmoSlot = CurrentAmmoSlot;
}
