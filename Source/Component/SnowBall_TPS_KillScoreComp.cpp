// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBall_TPS_KillScoreComp.h"
#include "Component/SnowBall_TPSHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Game/SnowBall_TPSGameMode.h"

// Sets default values for this component's properties
USnowBall_TPS_KillScoreComp::USnowBall_TPS_KillScoreComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USnowBall_TPS_KillScoreComp::BeginPlay()
{
	Super::BeginPlay();

	USnowBall_TPSHealthComponent* HealthComponent = Cast<USnowBall_TPSHealthComponent>(GetOwner()->GetComponentByClass(USnowBall_TPSHealthComponent::StaticClass()));

	if(HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &USnowBall_TPS_KillScoreComp::DeadEvent);
	}
	// ...
	
}


// Called every frame
void USnowBall_TPS_KillScoreComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USnowBall_TPS_KillScoreComp::DeadEvent()
{
	ASnowBall_TPSGameMode* myGameMode = Cast<ASnowBall_TPSGameMode>(UGameplayStatics::GetGameMode(this));
	if (myGameMode)
	{
		myGameMode->EnemyDead(Score);
	}
}

void USnowBall_TPS_KillScoreComp::Destroy()
{
	DestroyComponent();
}

