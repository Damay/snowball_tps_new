// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/SnowBall_TPSHealthComponent.h"

#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
USnowBall_TPSHealthComponent::USnowBall_TPSHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void USnowBall_TPSHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USnowBall_TPSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float USnowBall_TPSHealthComponent::GetCurrentHealth()
{
	return Health;
}

void USnowBall_TPSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool USnowBall_TPSHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void USnowBall_TPSHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{
	if (bIsAlive == true)
	{
		if (ChangeValue < 0.0f)
			Health += ChangeValue * CoefDamage;
		else
			Health += ChangeValue * CoefHealing;

		HealthChangeEvent_Multicast(Health, ChangeValue);

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health <= 0.0f)
			{
				DeadEvent_Multicast();
				bIsAlive = false;
			}
		}
	}
}

void USnowBall_TPSHealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void USnowBall_TPSHealthComponent::HealthChangeEvent_Multicast_Implementation(float newHealthValue, float DamageValue)
{
	OnHealthChange.Broadcast(newHealthValue, DamageValue);
}


void USnowBall_TPSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USnowBall_TPSHealthComponent, Health);
	DOREPLIFETIME(USnowBall_TPSHealthComponent, bIsAlive);
}