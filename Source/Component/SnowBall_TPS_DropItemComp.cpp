// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBall_TPS_DropItemComp.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy/SnowBall_TPS_EnemyCharacterBase.h"
#include "Game/SnowBall_TPSGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PickUp/SnowBall_TPS_PickUpActor_Ammo.h"

// Sets default values for this component's properties
USnowBall_TPS_DropItemComp::USnowBall_TPS_DropItemComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void USnowBall_TPS_DropItemComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

FName USnowBall_TPS_DropItemComp::FindWeaponName(EWeaponType WeaponTypeForFinding)
{
	FName WeaponName;

	switch (WeaponTypeForFinding)
	{
	case EWeaponType::GrenadeType:
		WeaponName = "Grenade";
		break;
	case EWeaponType::GunType:
		WeaponName = "Gun";
		break;
	case EWeaponType::PistolType:
		WeaponName = "Pistol";
		break;
	case EWeaponType::RifleType:
		WeaponName = "Rifle";
		break;
	case EWeaponType::TraceType:
		WeaponName = "Trace";
		break;
	default:
		WeaponName = "Pistol";
		break;
	}

	return WeaponName;
}


// Called every frame
void USnowBall_TPS_DropItemComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USnowBall_TPS_DropItemComp::Drop()
{
	FHitResult Hit;
	FVector VectorEnd = GetOwner()->GetActorLocation() - FVector(0.0f, 0.0f, 200.0f);
	const TArray<AActor*> Enemy;

	UKismetSystemLibrary::LineTraceSingle(GetOwner(), GetOwner()->GetActorLocation(), VectorEnd, ETraceTypeQuery::TraceTypeQuery1,
		false, Enemy, EDrawDebugTrace::None, Hit, true, FLinearColor(FColor(1, 0, 0, 255)), 
		FLinearColor(FColor(0, 1, 0, 255)), 20.0f);

	USnowBall_TPSGameInstance* GameInstance = Cast<USnowBall_TPSGameInstance>(UGameplayStatics::GetGameInstance(this));
	if (GameInstance)
	{
		FVector SpawnLocation = Hit.Location;
		FTransform SpawnTransform{ SpawnLocation };

		ASnowBall_TPS_PickUpActor_Ammo* AmmoPickUp = Cast<ASnowBall_TPS_PickUpActor_Ammo>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ASnowBall_TPS_PickUpActor_Ammo::StaticClass(), SpawnTransform));
		if(AmmoPickUp)	
		{
			FDropItem DropItem;
			FWeaponInfo WeaponInfo;
			GameInstance->GetDropItemInfoByName(FindWeaponName(WeaponType), DropItem);
			GameInstance->GetWeaponInfoByName(FindWeaponName(WeaponType), WeaponInfo);

			FDropItem MakeDropItem;
			MakeDropItem.WeaponStaticMesh = WeaponInfo.MagazineDrop.DropMesh;
			MakeDropItem.WeaponInfo = DropItem.WeaponInfo;

			AmmoPickUp->InitActor(MakeDropItem);
			AmmoPickUp->InitParams(Cout, WeaponType);

			UGameplayStatics::FinishSpawningActor(AmmoPickUp, SpawnTransform);


		}
	}

	DestroyComponent();
}