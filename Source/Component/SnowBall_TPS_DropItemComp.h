// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "SnowBall_TPS_DropItemComp.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SNOWBALL_TPS_API USnowBall_TPS_DropItemComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USnowBall_TPS_DropItemComp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	FName FindWeaponName(EWeaponType WeaponTypeForFinding);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, Category="DropItem")
		EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, Category = "DropItem")
		int32 Cout = 50;

	UFUNCTION(BlueprintCallable)
		void Drop();
};
