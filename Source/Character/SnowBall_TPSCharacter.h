// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapon/Weapon/WeaponDefault.h"
#include "FuncLibrary/Types.h"
#include "Engine/StaticMeshActor.h"
#include "SnowBall_TPSInventoryComponent.h"
#include "Character/SnowBall_TPSCharHealthComponent.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "StateEffect/SnowBallTPS_StateEffect.h"
#include "PickUp/SnowBall_TPS_PickUpActor_Weapon.h"
#include "SnowBall_TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ASnowBall_TPSCharacter : public ACharacter, public ISnowBallTPS_IGameActor
{
	GENERATED_BODY()

protected:

	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	virtual void BeginPlay() override;

	UFUNCTION()
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	ASnowBall_TPSCharacter();

	FTimerHandle TimerHandle_RacollTimer;
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	FORCEINLINE class USnowBall_TPSInventoryComponent* GetInventoryComponent() const { return InventoryComponent; }
	FORCEINLINE class USnowBall_TPSCharHealthComponent* GetCharHealthComponent() const { return CharHealthComponent; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	class USnowBall_TPSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
	class USnowBall_TPSCharHealthComponent* CharHealthComponent;


public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UDecalComponent* CurrentCursor = nullptr;

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		UAnimMontage* AnimRollMontage;

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Start state", Replicated)
	EMovementState MovementState = EMovementState::Base_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(BlueprintReadWrite)
		bool RunEnable = false;
	UPROPERTY(BlueprintReadWrite)
		bool WalkEnable = false;
	UPROPERTY(Replicated, BlueprintReadWrite)
		bool AimEnable = false;
	UPROPERTY(BlueprintReadWrite)
		bool RollEnable = false;
	UPROPERTY(BlueprintReadWrite)
		float RollTime = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxRollTime = 0.9f;
	UPROPERTY(BlueprintReadWrite)
		bool MoveSprint = false;


	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//Effect
	UPROPERTY(Replicated)
	TArray<USnowBallTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		USnowBallTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		USnowBallTPS_StateEffect* EffectRemove = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffect")
	TArray<FMaxCoutStateEffects> MaxCoutStateEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticalSystemComponents;

	//Input
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();
	UFUNCTION()
		void InputRollPressed();
	UFUNCTION()
		void InputWalkPressed();
	UFUNCTION()
		void InputWalkReleased();
	UFUNCTION()
		void InputRunPressed();
	UFUNCTION()
		void InputRunReleased();
	UFUNCTION()
		void InputAimPressed();
	UFUNCTION()
		void InputAimReleased();

	UFUNCTION(Server, Reliable)
		void AnimRollMovement_OnServer();
	UFUNCTION(NetMulticast, Reliable)
		void AnimRollMovement_Multicast();

	//Weapon
	UFUNCTION()
		void TryPickUpWeapon();
	UFUNCTION()
		void TryReloadWeapon();
	UFUNCTION(Server, Reliable, BlueprintCallable)
		void TrySwitchWeapon_OnServer();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSpent);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess, int32 AmmoSpent);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	void StartSwitchWeapon(ASnowBall_TPS_PickUpActor_Weapon* Weapon);
	UFUNCTION(BlueprintNativeEvent)
		void StartSwitchWeapon_BP();

	UFUNCTION(BlueprintCallable)
		void EndSwitchWeapon();
	UFUNCTION(BlueprintNativeEvent)
		void EndSwitchWeapon_BP();

	ASnowBall_TPS_PickUpActor_Weapon* OverlapPickUpWeapon = nullptr;

	//Ability
	UFUNCTION()
	void TryAbilityEnabled();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityEffect")
	TSubclassOf<USnowBallTPS_StateEffect> AbilityEffect;

	//Tick function
	UFUNCTION()
	void MovementTick(float DeltaTime);


	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void AimCameraReset();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFire);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CoefCameraOffset = 100.0f;

	UPROPERTY(BlueprintReadWrite)
	FVector NewRalativeLocation;

	//Weapon
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimMontage")
	TArray<UAnimMontage*> DeathMontage;

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	//Shoot
	FVector MouseLocation;
	FVector LineEnd;

	//Magazine animation
	UFUNCTION(BlueprintCallable)
	void AnimationDropMagazine();
	UFUNCTION(BlueprintCallable)
	void AnimationDestroyMagazine();
	AStaticMeshActor* MagazineMeshAnimation = nullptr;

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	UFUNCTION(BlueprintCallable)
	TArray<USnowBallTPS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(USnowBallTPS_StateEffect* RemoveEffect) override;
	void AddEffect(USnowBallTPS_StateEffect* NewEffect) override;
	bool CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass) override;
	FName GetNameBoneForStateEffect(FTransform& EmitterTransform);
	void DropWeaponToWorld(FDropItem DropItemInfo) override;

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void SwitchEffect(USnowBallTPS_StateEffect* Effect, bool bIsAdd);
	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ParticleSystem);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ParticleSystem);
	//EndInterface

	//UFUNCTION(Server, Reliable)
	//	void DropWeaponOnSwitch_OnServer(FDropItem DropItem);

	void DropSuccess();

	UFUNCTION()
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRacDoll_Multicast();

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(Server, Unreliable)
		void SetActorRotetionByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotetionByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void WeaponReloadAnimStart_Multicast(UAnimMontage* WeaponReloadAnim);
	UFUNCTION(NetMulticast, Reliable)
		void WeaponFireAnimStart_Multicast(UAnimMontage* WeaponFireAnim);
	UFUNCTION(NetMulticast, Reliable)
		void WeaponReloadAnimStop_Multicast();

	UFUNCTION(Server, Reliable)
		void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
		void PlayMontage_Multicast(UAnimMontage* AnimMontage);
};