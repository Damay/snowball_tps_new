// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnowBall_TPSCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Game/SnowBall_TPSGameInstance.h"
#include "Math/UnrealMathUtility.h"
#include "Components/SceneComponent.h"
#include "Animation/AnimSequenceBase.h"
#include "Components/StaticMeshComponent.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"
#include "Game/SnowBall_TPSGameMode.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"


ASnowBall_TPSCharacter::ASnowBall_TPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1000.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<USnowBall_TPSInventoryComponent>(TEXT("InventoryComponent"));


	if (InventoryComponent)
	{
		InventoryComponent->OnSwichWeapon.AddDynamic(this, &ASnowBall_TPSCharacter::InitWeapon);
	}

	CharHealthComponent = CreateDefaultSubobject<USnowBall_TPSCharHealthComponent>(TEXT("CharHealthComponent"));
	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ASnowBall_TPSCharacter::CharDead);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Network
	bReplicates = true;
}

void ASnowBall_TPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorLocation = TraceHitResult.ImpactNormal;
			FRotator CursorRotation = CursorLocation.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorRotation);
		}
	}
	MovementTick(DeltaSeconds);
}

void ASnowBall_TPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && (GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority))
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void ASnowBall_TPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ASnowBall_TPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ASnowBall_TPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::InputAttackReleased);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::TryAbilityEnabled);
	
	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::TrySwitchWeapon_OnServer);
	NewInputComponent->BindAction(TEXT("PickUpWeapon"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::TryPickUpWeapon);

	NewInputComponent->BindAction(TEXT("MovementModeChangeRoll"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::InputRollPressed);
	NewInputComponent->BindAction(TEXT("MovementModeChangeWalk"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::InputWalkPressed);
	NewInputComponent->BindAction(TEXT("MovementModeChangeWalk"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::InputWalkReleased);
	NewInputComponent->BindAction(TEXT("MovementModeChangeRun"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::InputRunPressed);
	NewInputComponent->BindAction(TEXT("MovementModeChangeRun"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::InputRunReleased);
	NewInputComponent->BindAction(TEXT("MovementModeChangeAim"), EInputEvent::IE_Pressed, this, &ASnowBall_TPSCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("MovementModeChangeAim"), EInputEvent::IE_Released, this, &ASnowBall_TPSCharacter::InputAimReleased);

}

UDecalComponent* ASnowBall_TPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ASnowBall_TPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ASnowBall_TPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ASnowBall_TPSCharacter::InputAttackPressed()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		AttackCharEvent(true);
	}

}

void ASnowBall_TPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ASnowBall_TPSCharacter::InputRollPressed()
{
	AnimRollMovement_OnServer();

	RollEnable = true;
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::InputWalkPressed()
{
	WalkEnable = true;
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::InputWalkReleased()
{
	WalkEnable = false;
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::InputRunPressed()
{
	RunEnable = true;
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::InputRunReleased()
{
	RunEnable = false;
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::InputAimPressed()
{
	AimEnable = true;
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::InputAimReleased()
{
	AimEnable = false;
	AimCameraReset();
	ChangeMovementState();
}

void ASnowBall_TPSCharacter::AnimRollMovement_OnServer_Implementation()
{
	AnimRollMovement_Multicast();
}

void ASnowBall_TPSCharacter::AnimRollMovement_Multicast_Implementation()
{
	PlayAnimMontage(AnimRollMontage, 2.3f, "Mesh");
}

void ASnowBall_TPSCharacter::TryReloadWeapon()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && CurrentWeapon && CurrentWeapon->WeaponReloading == false)
	{
		TryReloadWeapon_OnServer();
	}
}

void ASnowBall_TPSCharacter::TrySwitchWeapon_OnServer_Implementation()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;

			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}

			if (InventoryComponent)
			{
				InventoryComponent->SwitchWeaponToIndex(OldIndex, OldInfo);
			}
		}
	}
}

void ASnowBall_TPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadAnimStart_Multicast(Anim);
	WeaponReloadStart_BP(Anim);
}

void ASnowBall_TPSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSpent)
{
	if (!bIsSuccess)
		WeaponReloadAnimStop_Multicast();

	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSpent);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess, AmmoSpent);
}

void ASnowBall_TPSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);

	WeaponFireAnimStart_Multicast(Anim);

	WeaponFireStart_BP(Anim);
}

void ASnowBall_TPSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ASnowBall_TPSCharacter::StartSwitchWeapon(ASnowBall_TPS_PickUpActor_Weapon* Weapon)
{
	OverlapPickUpWeapon = Weapon;

	StartSwitchWeapon_BP();
}

void ASnowBall_TPSCharacter::EndSwitchWeapon()
{
	OverlapPickUpWeapon = nullptr;

	EndSwitchWeapon_BP();
}

void ASnowBall_TPSCharacter::EndSwitchWeapon_BP_Implementation()
{
}

void ASnowBall_TPSCharacter::StartSwitchWeapon_BP_Implementation()
{
}

void ASnowBall_TPSCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		USnowBallTPS_StateEffect* NewEffect = NewObject<USnowBallTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None); 
		}
	}
}

void ASnowBall_TPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ASnowBall_TPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess, int32 AmmoSpent)
{
	//in BP
}

void ASnowBall_TPSCharacter::MovementTick(float DeltaTime)
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

			if (myController)
			{
				myController->DeprojectMousePositionToWorld(MouseLocation, LineEnd);
			}

			LineEnd = (LineEnd * 5000) + MouseLocation;
			const FVector PointUnderCursor = FMath::LinePlaneIntersection(MouseLocation, LineEnd, GetActorLocation(), FVector(0, 0, 1));

			const float RotatingToCursor = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), PointUnderCursor).Yaw;

			FString SEnum = UEnum::GetValueAsString(GetMovementState());

			if (!RollEnable)
			{
				AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
				AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

				SetActorRotation(FQuat(FRotator(0.0f, RotatingToCursor, 0.0f)));

				SetActorRotetionByYaw_OnServer(RotatingToCursor);

				if (RunEnable)
				{
					const float ComparisonX = GetActorForwardVector().X;
					const float ComparisonY = GetActorForwardVector().Y;


					if (AxisX - 0.3f < ComparisonX && ComparisonX < AxisX + 0.3f
						&& AxisY - 0.3f < ComparisonY && ComparisonY < AxisY + 0.3f)
					{
						MoveSprint = true;
						ChangeMovementState();
					}
					else
					{
						MoveSprint = false;
						ChangeMovementState();
					}
				}

				if (AimEnable)
				{
					const FVector NewTransformLocation(GetActorLocation() + GetActorForwardVector() * CoefCameraOffset);
					NewRalativeLocation = FVector(NewTransformLocation.X, NewTransformLocation.Y, 198.150f);
					CameraBoom->SetWorldLocation(NewRalativeLocation);
				}

			}
			else
			{
				if (RollTime < MaxRollTime)
				{
					AddMovementInput(GetActorForwardVector());
					RollTime = RollTime + DeltaTime;
				}
				else
				{
 					CurrentWeapon->bIsRoll = false;
					RollEnable = false;
					RollTime = 0.0f;
					ChangeMovementState();
				}
			}
		}
	}
}

void ASnowBall_TPSCharacter::CharacterUpdate()
{
	float ResSpeed = MovementInfo.BaseSpeed;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Base_State:
		ResSpeed = MovementInfo.BaseSpeed;
		break;
	case EMovementState::Roll_State:
		ResSpeed = MovementInfo.RollSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ASnowBall_TPSCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_State;
	if (RollEnable)
	{
		NewState = EMovementState::Roll_State;
	}
	else
	{
		if (AimEnable && !RollEnable)
		{
			NewState = EMovementState::Aim_State;
		}
		else
		{
			if (WalkEnable && !AimEnable && !RollEnable)
			{
				NewState = EMovementState::Walk_State;
			}
			else
			{
				if (RunEnable && MoveSprint && !WalkEnable && !AimEnable && !RollEnable)
				{
					NewState = EMovementState::Run_State;
				}
				else
				{
					NewState = EMovementState::Base_State;

				}
			}
		}
	}

	SetMovementState_OnServer(NewState);

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

EMovementState ASnowBall_TPSCharacter::GetMovementState()
{
	return MovementState;
}

void ASnowBall_TPSCharacter::AttackCharEvent(bool bIsFire)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo check mele or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFire);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ASnowBall_TPSCharacter::AttackCharEvent - CurrentWeapon - NULL"));
}

AWeaponDefault* ASnowBall_TPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ASnowBall_TPSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{

	//OnSever
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	USnowBall_TPSGameInstance* myGI = Cast<USnowBall_TPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawmParams;
				SpawmParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawmParams.Owner = this;
				SpawmParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation,
					&SpawnRotation, SpawmParams));

				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->myPawn = this;
					myWeapon->bIsPalyer = true;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;

					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->WeaponInfo = WeaponAdditionalInfo;

					//if (InventoryComponent)
						CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ASnowBall_TPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ASnowBall_TPSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ASnowBall_TPSCharacter::WeaponFireStart);

					if (myWeapon->WeaponInfo.Round <=0)
					{
						TryReloadWeapon();
					}
				}
			}
		}
	}
}

void ASnowBall_TPSCharacter::AnimationDropMagazine()
{
	FTransform Transform;
	FActorSpawnParameters Param;

	Transform.SetRotation(CurrentWeapon->WeaponSetting.MagazineAnimRotator.Quaternion());

	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	MagazineMeshAnimation = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);
	MagazineMeshAnimation->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
	MagazineMeshAnimation->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("NoCollision"));
	MagazineMeshAnimation->GetStaticMeshComponent()->SetStaticMesh(CurrentWeapon->WeaponSetting.MagazineDrop.DropMesh);

	MagazineMeshAnimation->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "LeftHand");
}

void ASnowBall_TPSCharacter::AnimationDestroyMagazine()
{
	MagazineMeshAnimation->Destroy();
}


EPhysicalSurface ASnowBall_TPSCharacter::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;

	if (CharHealthComponent)
	{
		if (GetMesh())
		{
			UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
			if (myMaterial)
			{
				result = myMaterial->GetPhysicalMaterial()->SurfaceType;
			}
		}
	}
	return result;
}

TArray<USnowBallTPS_StateEffect*> ASnowBall_TPSCharacter::GetCurrentEffects()
{
	return Effects;
}

void ASnowBall_TPSCharacter::RemoveEffect(USnowBallTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyPartical)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ASnowBall_TPSCharacter::AddEffect(USnowBallTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyPartical)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticalEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticalEffect);
		}
	}

}

bool ASnowBall_TPSCharacter::CanAddStateEffect(TSubclassOf<USnowBallTPS_StateEffect> AddEffectClass)
{
	bool result = true;
	int i = 0;
	int32 MaxCoutEffect = 0;
	while (i < MaxCoutStateEffect.Num())
	{
		if (AddEffectClass == MaxCoutStateEffect[i].StateEffect)
		{
			MaxCoutEffect = MaxCoutStateEffect[i].Cout;
		}

		i++;
	}

	i = 0;
	int32 CoutEffect = 0;
	while (i < Effects.Num() && result == true)
	{
		if (Effects[i]->GetClass() == AddEffectClass)
		{
			CoutEffect++;
			if (CoutEffect > MaxCoutEffect)
			{
				result = false;
			}
		}
		i++;
	}

	return result;
}

FName ASnowBall_TPSCharacter::GetNameBoneForStateEffect(FTransform& EmitterTransform)
{
	FName BoneName = "none";

	if (GetMesh())
	{
		BoneName = GetMesh()->GetBoneName(0);
		EmitterTransform = GetMesh()->GetBoneTransform(0);
	}
	return BoneName;
}

void ASnowBall_TPSCharacter::DropWeaponToWorld(FDropItem DropItemInfo)
{
	DropSuccess();

	FVector SpawnLocation = FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - 96);
	FTransform SpawnTransform{ SpawnLocation };

	ASnowBall_TPS_PickUpActor_Weapon* DropWeapon = Cast<ASnowBall_TPS_PickUpActor_Weapon>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ASnowBall_TPS_PickUpActor_Weapon::StaticClass(), SpawnTransform));
	if (DropWeapon)
	{
		DropWeapon->InitActor(DropItemInfo);

		UGameplayStatics::FinishSpawningActor(DropWeapon, SpawnTransform);
	}
	EndSwitchWeapon();
}

void ASnowBall_TPSCharacter::DropSuccess()
{
	OverlapPickUpWeapon->PickUpSuccess();
}

void ASnowBall_TPSCharacter::TryPickUpWeapon()
{
	if(IsValid(OverlapPickUpWeapon))
	{
		InventoryComponent->SwitchWeaponToInventory_OnServer(OverlapPickUpWeapon->WeaponSlot, CurrentIndexWeapon);
	}
}

void ASnowBall_TPSCharacter::CharDead()
{
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;

		int32 i = FMath::RandRange(0, DeathMontage.Num() - 1);

		if (DeathMontage.IsValidIndex(i) && DeathMontage[i] && GetMesh()->GetAnimInstance() && GetMesh())
		{
			TimeAnim = DeathMontage[i]->GetPlayLength();
			PlayMontage_Multicast(DeathMontage[i]);
		}

		if (GetController())
		{
			auto PlayerController = Cast<ASnowBall_TPSPlayerController>(GetController());
			PlayerController->UnPossess();
			PlayerController->CharDead();
		}

		//Timer racdoll
		GetWorldTimerManager().SetTimer(TimerHandle_RacollTimer, this, &ASnowBall_TPSCharacter::EnableRacDoll_Multicast, TimeAnim, false);

		GetCurrentWeapon()->SetLifeSpan(TimeAnim);
		SetLifeSpan(TimeAnim);
	}
	else
	{
		if (GetCursorToWorld())
			GetCursorToWorld()->SetVisibility(false);

		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}

	CharDead_BP();
}

void ASnowBall_TPSCharacter::EnableRacDoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ASnowBall_TPSCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		CharHealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType()); //to do name_none
		}
	}
	return 	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void ASnowBall_TPSCharacter::EffectAdd_OnRep()
{
	if(EffectAdd)
		SwitchEffect(EffectAdd, true);
}

void ASnowBall_TPSCharacter::EffectRemove_OnRep()
{
	if(EffectRemove)
		SwitchEffect(EffectRemove, false);
}

void ASnowBall_TPSCharacter::SwitchEffect(USnowBallTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticalEffect)
		{
			FName NameBoneToAttach = Effect->NameBone;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UParticleSystemComponent* myParticalSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticalEffect, myMesh, NameBoneToAttach,
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticalSystemComponents.Add(myParticalSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticalEffect)
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticalSystemComponents.Num() > 0)
			{
				while (i < ParticalSystemComponents.Num(), !bIsFind)
				{
					if (ParticalSystemComponents[i]->Template && Effect->ParticalEffect && ParticalSystemComponents[i]->Template == Effect->ParticalEffect)
					{
						ParticalSystemComponents[i]->DeactivateSystem();
						ParticalSystemComponents[i]->DestroyComponent();
						ParticalSystemComponents.RemoveAt(i);

						bIsFind = true;
					}
				}
			}
		}
	}
}

void ASnowBall_TPSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ParticleSystem)
{
	ExecuteEffectAdded_Multicast(ParticleSystem);
}

void ASnowBall_TPSCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ParticleSystem)
{
	UTypes::ExecuteEffectAdded(ParticleSystem, this, FVector(0), FName("spine_01"));
}

void ASnowBall_TPSCharacter::PlayMontage_Multicast_Implementation(UAnimMontage* AnimMontage)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(AnimMontage);
	}
}

void ASnowBall_TPSCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if ((CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound) && CurrentWeapon->CheckWeaponReload())
	{
		CurrentWeapon->StartReload();
	}
}

void ASnowBall_TPSCharacter::WeaponReloadAnimStop_Multicast_Implementation()
{
	StopAnimMontage();
}

void ASnowBall_TPSCharacter::WeaponFireAnimStart_Multicast_Implementation(UAnimMontage* WeaponFireAnim)
{
	PlayAnimMontage(WeaponFireAnim, 1.0f, "Mesh");
}

void ASnowBall_TPSCharacter::WeaponReloadAnimStart_Multicast_Implementation(UAnimMontage* WeaponReloadAnim)
{
	PlayAnimMontage(WeaponReloadAnim, 1.0f, "Mesh");
}

void ASnowBall_TPSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ASnowBall_TPSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ASnowBall_TPSCharacter::SetActorRotetionByYaw_Multicast_Implementation(float Yaw)
{
	if (GetController() && !GetController()->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ASnowBall_TPSCharacter::SetActorRotetionByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotetionByYaw_Multicast(Yaw);
}

void ASnowBall_TPSCharacter::CharDead_BP_Implementation()
{
	//To BP
}

void ASnowBall_TPSCharacter::AimCameraReset()
{
	CameraBoom->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
}

bool ASnowBall_TPSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i<Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return Wrote;
}

void ASnowBall_TPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASnowBall_TPSCharacter, MovementState);
	DOREPLIFETIME(ASnowBall_TPSCharacter, CurrentWeapon);
	DOREPLIFETIME(ASnowBall_TPSCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ASnowBall_TPSCharacter, Effects);
	DOREPLIFETIME(ASnowBall_TPSCharacter, EffectAdd);
	DOREPLIFETIME(ASnowBall_TPSCharacter, EffectRemove);
	DOREPLIFETIME(ASnowBall_TPSCharacter, AimEnable);
}
