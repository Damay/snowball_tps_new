// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/SnowBall_TPSInventoryComponent.h"
#include "SnowBall_TPSCharacter.h"
#include "Interface/SnowBallTPS_IGameActor.h"
#include "Game/SnowBall_TPSGameInstance.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
USnowBall_TPSInventoryComponent::USnowBall_TPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void USnowBall_TPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void USnowBall_TPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool USnowBall_TPSInventoryComponent::SwitchWeaponToIndex(int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	int8 ChangedWeapon = 0;
	
	FName NewNameWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	int32 ChangeToIndex = OldIndex + 1;

	WeaponSlots[OldIndex].AdditionalInfo = OldInfo;

	/*if (IndexWeaponToSwitch > MaxSlotWeapon)
	{*/
	while (!bIsSuccess && ChangedWeapon < WeaponSlots.Num())
	{
		if (ChangeToIndex == WeaponSlots.Num())
		{
			ChangeToIndex = 0;
		}

		if (!WeaponSlots[ChangeToIndex].NameItem.IsNone())
		{
			FWeaponInfo myInfo;

			USnowBall_TPSGameInstance* myGi = Cast<USnowBall_TPSGameInstance>(GetWorld()->GetGameInstance());

			if (myGi)
			{
				myGi->GetWeaponInfoByName(WeaponSlots[ChangeToIndex].NameItem, myInfo);

				int8 IndexWeaponAmmoSlot = 0;
				while (!bIsSuccess && IndexWeaponAmmoSlot < AmmoSlots.Num())
				{
					if (AmmoSlots[IndexWeaponAmmoSlot].WeaponType == myInfo.WeaponType)
					{
						if (AmmoSlots[IndexWeaponAmmoSlot].Cout != 0 || WeaponSlots[ChangeToIndex].AdditionalInfo.Round != 0)
						{
							NewNameWeapon = WeaponSlots[ChangeToIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[ChangeToIndex].AdditionalInfo;
							bIsSuccess = true;
						}
					}
					IndexWeaponAmmoSlot++;
				}
			}
		}

		ChangedWeapon++;
		ChangeToIndex++;
		}

	if (!bIsSuccess)
	{
		//What will do, if switch not success
	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		SwitchWeapon_OnServer(NewNameWeapon, NewAdditionalInfo, ChangeToIndex - 1);
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo USnowBall_TPSInventoryComponent::GetAdditionalinfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;

	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT(" FAdditionalWeaponInfo USnowBall_TPSInventoryComponent::GetAdditionalinfoWeapon - No found weapon with inex - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT(" FAdditionalWeaponInfo USnowBall_TPSInventoryComponent::GetAdditionalinfoWeapon - Not correct index weapon - %d"), IndexWeapon);

	return result;
}

int32 USnowBall_TPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeapon)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeapon)
		{
			result = i;
			bIsFind = true;
		}
		i++;
	}

	return result;
}

FName USnowBall_TPSInventoryComponent::GetWeaponNameSlotByIndex(int32 IndexWeapon)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		return WeaponSlots[IndexWeapon].NameItem;
	}
	return "";
}

void USnowBall_TPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;

		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				WeaponAdditionalInfoChange_Multicast(IndexWeapon, NewInfo);
				//OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT(" USnowBall_TPSInventoryComponent::SetAdditionalInfoWeapon - No found weapon with inex - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT(" USnowBall_TPSInventoryComponent::SetAdditionalInfoWeapon - Not correct index weapon - %d"), IndexWeapon);
}

void USnowBall_TPSInventoryComponent::AmmoSlotChangeValue(EWeaponType WeaponType, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int i = 0;
	
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == WeaponType)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;

			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;

			if (AmmoSlots[i].Cout < 0)
			{
				AmmoSlots[i].Cout = 0;
			}

			AmmoChange_Multicast(WeaponType, AmmoSlots[i].Cout);

			if (CoutChangeAmmo > 0)
				AddedAmmo_Multicast(WeaponType);

			bIsFind = true;
		}
		i++;
	}
}

bool USnowBall_TPSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 & AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;

	bool bIsFind = false;
	int32 i = 0;

	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;

			AvailableAmmoForWeapon = AmmoSlots[i].Cout;

			if (AmmoSlots[i].Cout > 0)
				return true;
		}

		i++;
	}

	return false;
}

bool USnowBall_TPSInventoryComponent::CheckForSimilarWeapon(FWeaponSlot NewWeapon)
{
	int i = 0;
	while (i < WeaponSlots.Num())
	{
		if (WeaponSlots[i].NameItem == NewWeapon.NameItem)
		{
			return false;
		}
		i++;
	}

	return true;
}

bool USnowBall_TPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
		{
			result = true;
		}

		i++;
	}
	return result;
}

bool USnowBall_TPSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;

	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			FreeSlot = i;
			bIsFreeSlot = true;
		}
		i++;
	}
	return bIsFreeSlot;
}

void USnowBall_TPSInventoryComponent::SwitchWeaponToInventory_OnServer_Implementation(FWeaponSlot NewWeapon,
	int32 IndexSlot)
{
	FDropItem DropItemInfo;

	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropitemInfoFromInventory(IndexSlot, DropItemInfo) && CheckForSimilarWeapon(NewWeapon))
	{
		WeaponSlots[IndexSlot] = NewWeapon;

		SwitchWeaponToIndex(IndexSlot, NewWeapon.AdditionalInfo);

		UpdateWeaponSlots_Multicast(IndexSlot, NewWeapon);

		ISnowBallTPS_IGameActor* Interface = Cast<ISnowBallTPS_IGameActor>(GetOwner());
		if(Interface)
		{
			Interface->DropWeaponToWorld(DropItemInfo);
		}
	}
}

bool USnowBall_TPSInventoryComponent::GetDropitemInfoFromInventory(int32 IndexDropSlot, FDropItem& DropItemInfo)
{
	bool result = false;
	FName DropItemName = GetWeaponNameSlotByIndex(IndexDropSlot);
	
	USnowBall_TPSGameInstance* myGi = Cast<USnowBall_TPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGi)
	{
		result = myGi->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexDropSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexDropSlot].AdditionalInfo;
		}

	}

	return result;
}

void USnowBall_TPSInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(
	ASnowBall_TPS_PickUpActor_Weapon* PickUp, FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;

	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			if (CheckForSimilarWeapon(NewWeapon))
			{
				WeaponSlots[IndexSlot] = NewWeapon;

				UpdateWeaponSlots_Multicast(IndexSlot, NewWeapon);

				PickUp->PickUpSuccess();
			}
		}
	}
}

TArray<FWeaponSlot> USnowBall_TPSInventoryComponent::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> USnowBall_TPSInventoryComponent::GetAmmoSlots()
{
	return AmmoSlots;
}


void USnowBall_TPSInventoryComponent::SwitchWeapon_OnServer_Implementation(FName NewWeaponNamee,
                                                                           FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	OnSwichWeapon.Broadcast(NewWeaponNamee, WeaponAdditionalInfo, NewCurrentIndexWeapon);
}

void USnowBall_TPSInventoryComponent::AddedAmmo_Multicast_Implementation(EWeaponType WeapoType)
{
	OnAddedAmmo.Broadcast(WeapoType);
}

void USnowBall_TPSInventoryComponent::UpdateWeaponSlots_Multicast_Implementation(int32 IndexSlotChange,
                                                                                 FWeaponSlot NewInfo)
{
	OnUpdateWeaponSlots.Broadcast(IndexSlotChange, NewInfo);
}

void USnowBall_TPSInventoryComponent::WeaponAdditionalInfoChange_Multicast_Implementation(int32 IndexSlot,
                                                                                          FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void USnowBall_TPSInventoryComponent::AmmoChange_Multicast_Implementation(EWeaponType TypeAmmo, int32 CoutAmmo)
{
	OnAmmoChange.Broadcast(TypeAmmo, CoutAmmo);
}

void USnowBall_TPSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots)
{
	WeaponSlots = NewWeaponSlots;
	AmmoSlots = NewAmmoSlots;

	MaxSlotWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			SwitchWeapon_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}

void USnowBall_TPSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USnowBall_TPSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(USnowBall_TPSInventoryComponent, AmmoSlots);

}
