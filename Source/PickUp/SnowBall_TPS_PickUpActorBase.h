// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"
#include "SnowBall_TPS_PickUpActorBase.generated.h"

UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_PickUpActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnowBall_TPS_PickUpActorBase();

	/** Returns SphereCollision subobject **/
	FORCEINLINE class USphereComponent* GetSphereCollisionComponent() const { return SphereCollision; }
	/** Returns StaticMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetStaticMeshComponent() const { return StaticMesh; }
	/** Returns SkeletalMesh subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetSkeletalMeshComponent() const { return SkeletalMesh; }

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereCollision;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* SkeletalMesh;

public:	

	UFUNCTION()
	virtual	void InitActor(FDropItem ItemInfo);

	UFUNCTION(BlueprintNativeEvent)
		void InitActor_BP();

	UFUNCTION(BlueprintCallable)
		void PickUpSuccess();

	UFUNCTION()
		virtual void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		virtual void CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};