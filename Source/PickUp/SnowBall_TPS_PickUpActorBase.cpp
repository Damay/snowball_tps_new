// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBall_TPS_PickUpActorBase.h"
#include "Components/SphereComponent.h"


// Sets default values
ASnowBall_TPS_PickUpActorBase::ASnowBall_TPS_PickUpActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCoolision"));
	if (SphereCollision)
	{
		SetRootComponent(SphereCollision);
		SphereCollision->SetSphereRadius(55.0f);
		SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ASnowBall_TPS_PickUpActorBase::CollisionSphereBeginOverlap);
		SphereCollision->OnComponentEndOverlap.AddDynamic(this, &ASnowBall_TPS_PickUpActorBase::CollisionSphereEndOverlap);
	}

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    if (StaticMesh)
    {
		StaticMesh->SetupAttachment(SphereCollision);
		StaticMesh->SetCollisionProfileName("NoCollision");
    }

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	if(SkeletalMesh)
	{
		SkeletalMesh->SetupAttachment(SphereCollision);
		SkeletalMesh->SetCollisionProfileName("NoCollision");
	}

	bReplicates = true;
}

void ASnowBall_TPS_PickUpActorBase::InitActor(FDropItem ItemInfo)
{

	if (IsValid(ItemInfo.WeaponStaticMesh))
	{
		SkeletalMesh->DestroyComponent();

		StaticMesh->SetStaticMesh(ItemInfo.WeaponStaticMesh);
		StaticMesh->SetRelativeLocation(ItemInfo.RelativeLocation);
	}
	else
	{
		StaticMesh->DestroyComponent();

		SkeletalMesh->SetSkeletalMesh(ItemInfo.WeaponSkeletalMesh);
		SkeletalMesh->SetRelativeLocation(ItemInfo.RelativeLocation);
	}

	InitActor_BP();
}

void ASnowBall_TPS_PickUpActorBase::PickUpSuccess()
{
	Destroy();
}

void ASnowBall_TPS_PickUpActorBase::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult& SweepResult)
{
}

void ASnowBall_TPS_PickUpActorBase::InitActor_BP_Implementation()
{
}

void ASnowBall_TPS_PickUpActorBase::CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}
