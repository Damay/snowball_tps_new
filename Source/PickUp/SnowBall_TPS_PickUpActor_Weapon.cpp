// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp/SnowBall_TPS_PickUpActor_Weapon.h"
#include "Character/SnowBall_TPSCharacter.h"
#include "Net/UnrealNetwork.h"


void ASnowBall_TPS_PickUpActor_Weapon::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                                                   AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                                                   const FHitResult& SweepResult)
{
	ASnowBall_TPSCharacter* Character = Cast<ASnowBall_TPSCharacter>(OtherActor);
	if (IsValid(Character))
	{
		Character->GetInventoryComponent()->TryGetWeaponToInventory_OnServer(this, WeaponSlot);

		int32 Freeslot;

		if (Character->GetInventoryComponent()->CheckForSimilarWeapon(WeaponSlot) && !Character->GetInventoryComponent()->CheckCanTakeWeapon(Freeslot))
		{
			Character->StartSwitchWeapon(this);
		}
	}
}

void ASnowBall_TPS_PickUpActor_Weapon::CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ASnowBall_TPSCharacter* Character = Cast<ASnowBall_TPSCharacter>(OtherActor);
	if (IsValid(Character))
	{
		Character->EndSwitchWeapon();
	}
}

void ASnowBall_TPS_PickUpActor_Weapon::InitActor(FDropItem ItemInfo)
{
	WeaponSlot = ItemInfo.WeaponInfo;

	Super::InitActor(ItemInfo);
}
