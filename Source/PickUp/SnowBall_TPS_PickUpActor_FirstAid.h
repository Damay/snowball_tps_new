// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnowBall_TPS_PickUpActorBase.h"
#include "SnowBall_TPS_PickUpActor_FirstAid.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_PickUpActor_FirstAid : public ASnowBall_TPS_PickUpActorBase
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
		TSubclassOf<USnowBallTPS_StateEffect> Effect;
public:

	virtual void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealthSetting")
		float ChangeValue = 30.0f;
};
