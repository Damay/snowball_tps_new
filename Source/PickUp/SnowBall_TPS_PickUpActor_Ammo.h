// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnowBall_TPS_PickUpActorBase.h"
#include "SnowBall_TPS_PickUpActor_Ammo.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_PickUpActor_Ammo : public ASnowBall_TPS_PickUpActorBase
{
	GENERATED_BODY()


public:

	virtual void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSetting")
		EWeaponType WeaponTypeAmmo = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSetting")
		int32 Cout = 30;

	void InitParams(int32 NewCout, EWeaponType NewWeaponTypeAmmo);
};
