// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnowBall_TPS_PickUpActorBase.h"
#include "SnowBall_TPS_PickUpActor_Weapon.generated.h"

/**
 * 
 */
UCLASS()
class SNOWBALL_TPS_API ASnowBall_TPS_PickUpActor_Weapon : public ASnowBall_TPS_PickUpActorBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		FWeaponSlot WeaponSlot;

	virtual void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	virtual	void InitActor(FDropItem ItemInfo) override;
};
