// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp/SnowBall_TPS_PickUpActor_FirstAid.h"
#include "Character/SnowBall_TPSCharacter.h"
#include "Kismet/GameplayStatics.h"


void ASnowBall_TPS_PickUpActor_FirstAid::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult& SweepResult)
{
	ASnowBall_TPSCharacter* Character = Cast<ASnowBall_TPSCharacter>(OtherActor);
	if (IsValid(Character))
	{
		if (Character->GetCharHealthComponent()->GetCurrentHealth() < 100.0f)
		{
			UTypes::AddEffectBySurfaceType(Character, FName("spine_01"), Effect, Character->GetSurfaceType());
			PickUpSuccess();
		}
	}
}