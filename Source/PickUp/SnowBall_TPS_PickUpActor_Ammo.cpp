// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp/SnowBall_TPS_PickUpActor_Ammo.h"
#include "Character/SnowBall_TPSCharacter.h"


void ASnowBall_TPS_PickUpActor_Ammo::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                                                 AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                                                 const FHitResult& SweepResult)
{
	ASnowBall_TPSCharacter* Character = Cast<ASnowBall_TPSCharacter>(OtherActor);
	if (IsValid(Character))
	{
		if (Character->GetInventoryComponent()->CheckCanTakeAmmo(WeaponTypeAmmo))
		{
			Character->GetInventoryComponent()->AmmoSlotChangeValue(WeaponTypeAmmo, Cout);

			PickUpSuccess();
		}
	}
}

void ASnowBall_TPS_PickUpActor_Ammo::InitParams(int32 NewCout, EWeaponType NewWeaponTypeAmmo)
{
	Cout = NewCout;
	WeaponTypeAmmo = NewWeaponTypeAmmo;
}

